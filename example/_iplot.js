init({
  width: 320,
  height: 240,
  margin: 0,
  dx: 40,
  dy: 40,
  offsetY: -1
});
grid();
nextColor(3);
rangeX(-3, 3);
xAxis(-3, 3);
yAxis(-1, 3);
plot(function(x) {
  return 1 / (1 + x * x);
}, [-2.5, 2.5], 0.1, 1);
setColor('#000');
label('x', 3, -.2);
label('y', -.25, 3.5);
label('0', -.25, -.2);
label('1', -.25, 1.35);
label('1', 0.75, -.2);
