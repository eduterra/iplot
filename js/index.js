
(function($) {
  // Simple JS-generated SVG
  $.bigfoot.install('script[type="application/x-iplot"]', function() {

    var scriptTag = $(this)
      , script = scriptTag.text();

    var cnt = $('<div/>')
      .addClass('iplotContainer')
      .insertAfter(scriptTag);

    var iplot = new IPlot(cnt);

    new Function('iplot', 'with (iplot) { ' + script + ' }')
      .call(iplot, iplot);
  });

  function IPlot(container) {
    this.container = d3.select(container[0]);
    this.color = IPlot.colors[0];
  }

  IPlot.colors = [
    d3.hsl(120, .5, .5).toString(),
    d3.hsl(200, .75, .6).toString(),
    d3.hsl(0, .75, .65).toString(),
    d3.hsl(230, .5, .6).toString(),
    d3.hsl(50, .7, .7).toString(),
    d3.hsl(160, .5, .7).toString(),
    d3.hsl(280, .5, .7).toString()
  ];

  IPlot.prototype.init = function(options) {
    options = options || {};
    this.width = options.width || 640;
    this.height = options.height || 480;
    this.margin = options.margin || 4;
    this.svg = this.container
      .append('svg')
      .attr('width', this.width + 2 * this.margin)
      .attr('height', this.height + 2 * this.margin)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');
    // Additional options for plotting
    this.cx = options.cx || (this.width / 2);
    this.cy = options.cy || (this.height / 2);
    this.dx = options.dx || 20;
    this.dy = options.dy || 20;
    this.offsetX = options.offsetX || 0;
    this.offsetY = options.offsetY || 0;
    this.cx = this.cx + (this.dx * this.offsetX);
    this.cy = this.cy - (this.dy * this.offsetY);
    this.xMax = this.width - this.width % this.dx;
    this.yMax = this.height - this.height % this.dy;
    // Unclamped (extrapolating) linear scales to avoid unnecesary calculations
    this.X = d3.scale.linear().domain([0, 1]).range([this.cx, this.cx + this.dx]);
    this.Y = d3.scale.linear().domain([0, 1]).range([this.cy, this.cy - this.dy]); // inverted
    // Drawing region
    this.region = {
      minX: this.X.invert(0),
      maxX: this.X.invert(this.xMax),
      minY: this.Y.invert(this.yMax),
      maxY: this.Y.invert(0)
    };
    // Update stickies
    $(window).trigger('sizeChanged');
  };

  IPlot.prototype.grid = function(dx, dy, classes) {
    var iplot = this;
    var grid = iplot.svg.append('g')
      .attr('class', 'grid');
    // Default ticks are used, if not specified
    classes = classes || '';
    dx = dx || iplot.dx;
    dy = dy || iplot.dy;
    // Generate ticks on entire space
    var xTicks = d3.range(0, iplot.xMax + dx, dx);
    var yTicks = d3.range(0, iplot.yMax + dy, dy);
    // Draw lines
    grid.selectAll('line.x')
      .data(xTicks)
      .enter()
      .append('line')
      .attr({
        class: 'x ' + classes,
        x1: function(d) { return d },
        x2: function(d) { return d },
        y1: 0,
        y2: iplot.yMax
      });
    grid.selectAll('line.y')
      .data(yTicks)
      .enter()
      .append('line')
      .attr({
        class: 'y ' + classes,
        x1: 0,
        x2: iplot.xMax,
        y1: function(d) { return d },
        y2: function(d) { return d }
      });
  };

  IPlot.prototype.xAxis = function(minX, maxX) {
    var iplot = this;
    iplot.region.minX = minX;
    iplot.region.maxX = maxX;
    var axis = iplot.svg
      .append('g')
      .attr('class', 'axis x');
    axis.append('line')
      .attr({
        class: 'axis x',
        x1: iplot.X(minX),
        x2: iplot.X(maxX),
        y1: iplot.cy,
        y2: iplot.cy
      });
    axis.selectAll('.tick.x')
      .data(d3.range(minX + 1, maxX))
      .enter()
      .append('line')
      .attr({
        class: 'tick x',
        x1: function(d) { return iplot.X(d) },
        x2: function(d) { return iplot.X(d) },
        y1: iplot.cy - 2,
        y2: iplot.cy + 2
      });
    axis.append('path')
      .attr('d', 'M' + (iplot.X(maxX) + 24) + ',' + iplot.cy +
      'l -28,-5 c 5,0 5,10 0,10z');
  };

  IPlot.prototype.yAxis = function(minY, maxY) {
    var iplot = this;
    iplot.region.minY = minY;
    iplot.region.maxY = maxY;
    var axis = iplot.svg
      .append('g')
      .attr('class', 'axis y');
    axis.append('line')
      .attr({
        class: 'axis y',
        x1: iplot.cx,
        x2: iplot.cx,
        y1: iplot.Y(minY),
        y2: iplot.Y(maxY)
      });
    axis.selectAll('.tick.y')
      .data(d3.range(minY + 1, maxY))
      .enter()
      .append('line')
      .attr({
        class: 'tick y',
        x1: iplot.cx - 2,
        x2: iplot.cx + 2,
        y1: function(d) { return iplot.Y(d) },
        y2: function(d) { return iplot.Y(d) }
      });
    axis.append('path')
      .attr('d', 'M' + iplot.cx + ',' + (iplot.Y(maxY) - 24) +
      'l -5,28 c 0,-5 10,-5 10,0z');
  };

  IPlot.prototype.rangeX = function(range) {
    if (Array.isArray(range) && range.length == 2)
      return range;
    return [this.region.minX, this.region.maxX];
  };

  IPlot.prototype.rangeY = function(range) {
    if (Array.isArray(range) && range.length == 2)
      return range;
    return [this.region.minY, this.region.maxY];
  };

  IPlot.prototype.setColor = function(color) {
    this.color = color;
  };

  IPlot.prototype.nextColor = function(incr) {
    incr = incr || 1;
    var i = IPlot.colors.indexOf(this.color);
    if (i > -1)
      this.color = IPlot.colors[(i + incr) % IPlot.colors.length];
    else
      this.color = IPlot.colors[0];
  };

  IPlot.prototype.point = function(x, y, empty) {
    var iplot = this;
    var point = iplot.svg.append('circle')
      .attr({
        class: 'point',
        stroke: iplot.color,
        cx: iplot.X(x),
        cy: iplot.Y(y),
        r: Math.max(4, iplot.dx / 10),
        fill: empty ? '#fff' : iplot.color,
        'stroke-width': 2
      });
    return point;
  };

  IPlot.prototype.line = function(point1, point2) {
    var iplot = this;
    var line = d3.svg.line()
      .x(function(d) { return iplot.X(d[0]) })
      .y(function(d) { return iplot.Y(d[1]) });
    return iplot.svg.append('path')
      .attr({
        class: 'line',
        stroke: iplot.color,
        d: line([point1, point2]),
        'stroke-width': 2
      });
  };

  IPlot.prototype.circle = function(center, radius) {
    var iplot = this;
    return iplot.svg.append('circle')
      .attr({
        class: 'circle',
        stroke: iplot.color,
        cx: iplot.X(center[0]),
        cy: iplot.Y(center[1]),
        r: iplot.dx * radius,
        fill: 'none',
        'stroke-width': 2
      });
  };

  IPlot.prototype.rect = function(point, width, height) {
    var iplot = this;
    return iplot.svg.append('rect')
      .attr({
        class: 'rect',
        stroke: iplot.color,
        x: iplot.X(point[0]),
        y: iplot.Y(point[1]),
        width: iplot.dx * width,
        height: iplot.dy * height,
        fill: 'none',
        'stroke-width': 2
      });
  };

  IPlot.prototype.plot = function(fn, range, step, duration) {
    var iplot = this;
    range = iplot.rangeX(range);
    var minX = range[0]
      , maxX = range[1];
    step = step || .1;
    duration = duration || 0;
    var points = d3.range(minX, maxX, step).concat([maxX]).map(function(x) {
      return {
        x: iplot.X(x),
        y: iplot.Y(fn(x))
      }
    });
    var line = d3.svg.line()
      .x(function(d) { return d.x })
      .y(function(d) { return d.y })
      .interpolate('basis');
    var graph = iplot.svg.append('path')
      .attr({
        class: 'graph',
        stroke: iplot.color,
        d: line(points),
        'stroke-width': 2
      });
    if (duration) {
      graph.transition()
        .duration(duration)
        .attrTween('d', function() {
          return function(t) {
            var p = points.slice(0, Math.round(points.length * t));
            return line(p) || 'M1,1';
          }
        });
    }
    return graph;
  };

  IPlot.prototype.area = function(fn1, fn2, range, step, duration) {
    var iplot = this;
    range = iplot.rangeX(range);
    var minX = range[0]
      , maxX = range[1];
    step = step || .1;
    duration = duration || 0;
    var points = d3.range(minX, maxX, step).concat([maxX]).map(function(x) {
      return {
        x: iplot.X(x),
        y0: iplot.Y(fn1(x)),
        y1: iplot.Y(fn2(x))
      }
    });
    var area = d3.svg.area()
      .x(function(d) { return d.x })
      .y0(function(d) { return d.y0 })
      .y1(function(d) { return d.y1 })
      .interpolate('basis');
    var graph = iplot.svg.append('path')
      .attr({
        class: 'area',
        fill: iplot.color,
        d: area(points),
        opacity:.2
      });
    if (duration) {
      graph.transition()
        .duration(duration)
        .attrTween('d', function() {
          return function(t) {
            var p = points.slice(0, Math.round(points.length * t));
            return area(p) || 'M1,1';
          }
        });
    }
    return area;
  };

  IPlot.prototype.label = function(text, x, y) {
    var iplot = this;
    var label = iplot.container.append('xhtml:div')
      .attr({
        class: 'label',
        style: [
          'color: ' + iplot.color,
          'top: ' + iplot.Y(y) + 'px',
          'left: ' + iplot.X(x) + 'px'
        ].join(';')
      })
      .text(text);
    if (typeof MathJax != "undefined")
      MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    return label;
  };
})(jQuery);

