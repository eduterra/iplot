'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jade: {
      compile: {
        options: {
          data: {
            pkg: grunt.file.readJSON('package.json')
          }
        },
        files: [{
          src: ['**/*.jade', '!**/_*.jade'],
          dest: 'build/<%= pkg.version %>/examples',
          ext: '.html',
          cwd: 'example/',
          expand: true
        }]
      }
    },

    uglify: {
      options: {
        compress: true
      },
      iplot: {
        files: {
          'build/<%= pkg.version %>/iplot.min.js': [ 'js/index.js' ]
        }
      }
    },

    stylus: {
      compile: {
        options: {
          compress: true
        },
        files: {
          'build/<%= pkg.version %>/iplot.css': 'css/iplot.styl'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-stylus');

  grunt.registerTask('default', ['uglify', 'stylus', 'jade']);

};
